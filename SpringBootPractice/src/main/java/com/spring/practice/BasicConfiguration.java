package com.spring.practice;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "basic")
public class BasicConfiguration {

	private boolean value;
	private int number;
	private String message;

	public boolean getValue() {
		return value;
	}
	public int getNumber() {
		return number;
	}
	public String getMessage() {
		return message;
	}
	public void setValue(boolean value) {
		this.value = value;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public void setMessage(String message) {
		this.message = message;
	}

}

/*
 * Note - @ConfigurationProperties provide by Spring boot,this annotation helps
 * in configured all class varible with properties file variable value.
 */

/*
 * All the required properties define in class and can be configured them from
 * properties file by this annotaion.Then we can Autowired in controller class.
 */
