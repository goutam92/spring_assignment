package com.spring.practice;

import org.springframework.stereotype.Component;

@Component
public class ComponentScanExm {
	public String getMessage1() {
		return "Component scan annotation example";
	}

}
