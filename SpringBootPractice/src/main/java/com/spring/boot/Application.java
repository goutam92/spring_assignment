package com.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.spring.practice.BasicConfiguration;

@SpringBootApplication
@ComponentScan("com.spring")
//@EnableConfigurationProperties(BasicConfiguration.class)
public class Application {
	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(Application.class, args);

	}
}

/*
 * Componet Scan defines where spring look for the component is declared,if we
 * dnt use Compnent scan then spring will look for the component on in tbe
 * package "com.spring.boot;" bcz @SpringBootApplication is declared in thos
 * package and its the entry point .
 */

//@Repository
