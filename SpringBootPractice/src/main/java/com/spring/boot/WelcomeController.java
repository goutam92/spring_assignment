package com.spring.boot;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.practice.BasicConfiguration;
import com.spring.practice.ComponentScanExm;

@RestController
public class WelcomeController {

	// WelcomeService obj = new WelcomeService();

	@Autowired
	ComponentScanExm obj1;
	@Autowired
	WelcomeService obj;

	@RequestMapping("/welcome")
	public String welcome() {
		return obj.getMessage() + " " + obj1.getMessage1();
	}

	@RequestMapping("/propData")
	public String printPropertyData() {
		return obj.getPropertyFileMsg();
	}

	@Autowired
	BasicConfiguration obj2;

	@RequestMapping("/cofigData")
	public Map<String, Object> printConfigPropertyData() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("message", obj2.getMessage());
		map.put("number", obj2.getNumber());
		map.put("value", obj2.getValue());
		System.out.println(map);
		return map;
	}
}

/* Once we declare this annotation,spring will create a instance for this */

// @Service
@Component
class WelcomeService {
	public String getMessage() {
		return "Wellcome to Spring Boot Practice..!!";
	}

	// retrieving data from application.properties example
	@Value("${welcome.message}")
	private String fileValue;

	public String getPropertyFileMsg() {
		return fileValue;
	}
}

/*
 * In the above exm, WelcomeController class depends on WelcomeService for
 * output to display , to spring will inject WelcomeService class instance
 * through annotation,DI
 */

// https://javarevisited.blogspot.com/2017/08/difference-between-restcontroller-and-controller-annotations-spring-mvc-rest.html